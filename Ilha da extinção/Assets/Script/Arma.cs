﻿using UnityEngine;
public class Arma : MonoBehaviour {
    public int danoFisico;
    private Status dono;
	void Start () {
		dono = transform.root.GetComponentInChildren<Status>();
	}
	float test(float dano,float defesa) {
		float a = defesa - dano;
		if (a < 0)
			a = 0;
		a = defesa - a;
		return a*0.01f;
	}
	void OnTriggerEnter (Collider collision) {
        if (dono.GetTypeAnimation(Type.Atak))
        {
            if (collision.transform.root.GetComponentInChildren<Status>() != null)
            {
                Status alvo = collision.transform.root.GetComponentInChildren<Status>();
                if (alvo.transform.GetInstanceID() != dono.transform.GetInstanceID())
                {
                    alvo.SetDano(danoFisico, dono.transform);
                }
            }
        }
	}
}
