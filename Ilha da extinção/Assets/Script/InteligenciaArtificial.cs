﻿using UnityEngine;
using UnityEngine.AI;

enum Mode { Passivo = 0, Neutro = 1, Agressivo = 2 };
enum Ordem { Herbivoro = 0, Onivoro = 1, Carnivoro = 2 };
enum Alimento { Agua=0,Carne=1,Planta=2};
enum Inteligencia {Baixo=0 }
[RequireComponent(typeof(Status))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public class InteligenciaArtificial : MonoBehaviour {
    [SerializeField] private Mode m_Mode = Mode.Neutro;
    [SerializeField] private Ordem m_Ordem = Ordem.Onivoro;
    [SerializeField] private Inteligencia m_NivelDeInteligencia = Inteligencia.Baixo;
    [SerializeField] private Transform[] m_Points=new Transform[0];
    [SerializeField] private LayerMask m_Layer = new LayerMask();
    public float RaioMax = 10,DistRun=5,DistMax=15,DistMin=1;
    private NavMeshAgent m_NavMeshAgent;
    private Status m_Status;
    [SerializeField] public Transform m_Alvo;
    void Start () {
        m_NavMeshAgent = GetComponent<NavMeshAgent>();
        m_Status = gameObject.GetComponent<Status>();
        Respaw[] a = FindObjectsOfType<Respaw>();
        m_Points= new Transform[a.Length];
        for (int i = 0; i < a.Length; i++)
        {
            m_Points[i] = a[i].transform;
        }
        m_NavMeshAgent.speed = m_Status.GetVelocidadeDeMovimento();
    }
	
	void FixedUpdate () {
        if (m_Status.GetVida()<=0)
        {
            Destroy(gameObject);
        }
        if (m_Alvo != null)
        {
            if (m_Alvo.GetComponent<Status>())
            {
                if (Vector3.Distance(m_Alvo.position, transform.position) > RaioMax)
                {
                    m_Alvo = null;
                    return;
                }
                Ataca(new Transform[] { m_Alvo });
                return;
            }
        }
        if (Mode.Agressivo==m_Mode)
        {
           RaycastHit[] hit=Physics.SphereCastAll(transform.position, 50, Vector3.down, 50, m_Layer, QueryTriggerInteraction.Ignore);
           if(hit.Length>0)
            {
                Transform[] alvos =new Transform[10];
                for (int i = 0; i < hit.Length; i++)
                {
                    if (hit[i].collider.transform.root != transform.root)
                    {
                        Transform alvo = hit[i].collider.transform.root.GetComponentInChildren<Status>()? hit[i].collider.transform.root.GetComponentInChildren<Status>().transform:null;
                        if (!alvo)
                            return;
                        if (alvo.name != name)
                        {
                            Vector3 relative = transform.InverseTransformPoint(alvo.position);
                            Vector2 angulo = new Vector2(Mathf.Abs(Mathf.Atan2(relative.x, relative.z) * Mathf.Rad2Deg), Mathf.Abs(Mathf.Atan2(relative.y, relative.z) * Mathf.Rad2Deg));
                            if (angulo.x < 60 && angulo.y < 60)
                            {
                                RaycastHit hits;
                                if (Physics.Linecast(transform.position, alvo.position, out hits))
                                {
                                    if (hits.collider.transform.root.name == alvo.name)
                                    {
                                        for(int j=0;j< alvos.Length; j++)
                                        {
                                            if (alvos[j] != null)
                                                if (alvos[j].GetInstanceID() == alvo.GetInstanceID())
                                                    return;
                                            if (alvos[j] == null)
                                                alvos[j] = alvo;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (alvos.Length > 0)
                {
                        Ataca(alvos);
                        return;
                }
            }
        }
        if (m_Status.GetTime() > 0)
        {
            switch (m_Mode) {
                case Mode.Passivo:
                    Fugi(m_Points);
                    return;
                case Mode.Neutro:
                    Ataca(m_Status.GetInimigo());
                    return;
                case Mode.Agressivo:
                    Ataca(m_Status.GetInimigo());
                    return;
            }
        }

        if (m_Alvo!=null)
        {
            if (m_Alvo.GetComponent<Status>()==null)
            {
                if (Vector3.Distance(m_Alvo.position, transform.position) > DistRun)
                {
                    Move();
                }
                else
                {
                    m_Alvo = null;
                }
            }
        }
        else
        {
            m_Alvo = m_Points[Random.Range(0, m_Points.Length)];
        }
    }
    private void Ataca(Transform[] enemy)
    {
        m_Alvo = enemy[0];
        switch (m_NivelDeInteligencia)
        {
            case Inteligencia.Baixo:
                VerificarDistancia(ref enemy);
                m_Alvo = enemy[0];
                break;
        }
        
        if (Vector3.Distance(transform.position, m_Alvo.position) <= DistMin)
        {
            m_Points[0].LookAt(m_Alvo);
            m_Points[0].eulerAngles = new Vector3(0, m_Points[0].eulerAngles.y, 0);
            if(m_Points[0].eulerAngles.y-transform.eulerAngles.y>20|| m_Points[0].eulerAngles.y - transform.eulerAngles.y < 20)
            {
                transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, m_Points[0].eulerAngles, Time.deltaTime);
            }
            m_NavMeshAgent.stoppingDistance = DistMin;
            m_Status.SetTrigger("Golpe");
            return;
        }
        else
        {
            Move();
        }
    }
    private void VerificarDistancia(ref Transform[] enemy)
    {
        Transform alvo=enemy[0];
        for(int i = 0; i < enemy.Length; i++)
        {
            if(IsDistance(enemy[0],enemy[i],transform))
            {
                alvo = enemy[0];
                enemy[0] = enemy[i];
                enemy[i] = alvo;
            }
        }
    }
    private bool IsDistance(Transform a_alvo, Transform b_alvo, Transform c_ponto)
    {
        if(Vector3.Distance(a_alvo.position, c_ponto.position) > Vector3.Distance(b_alvo.position, c_ponto.position))
        {
            return true;
        }
        return false;
    }
    private void Busca(Ordem ordem)
    {
        switch (m_Ordem)
        {
            case Ordem.Herbivoro:
                Busca(Alimento.Planta);
                return;
            case Ordem.Onivoro:
                Busca(new Alimento[] { Alimento.Carne, Alimento.Planta });
                return;
            case Ordem.Carnivoro:
                Busca(Alimento.Carne);
                return;
        }
    }
    private void OnDestroy()
    {
        transform.root.GetComponent<Respaw>().Insta();
    }
    private void Fugi(Transform[] ponto)
    {
        if (m_Alvo != null)
        {
            if (m_Alvo.GetComponent<Status>() == null)
            {
                if (Vector3.Distance(m_Alvo.position, transform.position) > DistRun)
                {
                    Move();
                }
                else
                {
                    m_Alvo = null;
                }
            }
        }
        else
        {
            m_Alvo = m_Points[Random.Range(0, m_Points.Length)];
        }
        Debug.Log("não esta fugindo");
    }
    private void Busca(Alimento[] ordem)
    {
        Debug.Log("não esta buscando");
    }
    private void Busca(Alimento ordem)
    {
        Debug.Log("não esta buscando o alimento");
    }
    private void Come(Alimento alimento)
    {
        Debug.Log("não esta comendo");
    }
    private void Move()
    {
        if (m_Status.GetTypeAnimation(Type.Move))
        {
            if (Vector3.Distance(transform.position, m_Alvo.position)>DistRun)
            {
                if (m_Status.SetFloat("Velocidade", 1))
                {
                    m_NavMeshAgent.destination = m_Alvo.position;
                    m_NavMeshAgent.speed =m_Status.GetVelocidadeDeMovimento();
                }
            }
            else if(Vector3.Distance(transform.position, m_Alvo.position) > DistMin)
            {
                if (m_Status.SetFloat("Velocidade", 0.5f))
                {
                    m_NavMeshAgent.destination = m_Alvo.position;
                    m_NavMeshAgent.speed = 0.5f;
                }
            }
            return;
        }
    }
}
