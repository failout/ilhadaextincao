﻿using UnityEngine;
using UnityStandardAssets.Utility;
[RequireComponent(typeof(Status))]
[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public class Player : MonoBehaviour
{
    [SerializeField] private float m_WalkSpeed = 1;
    [SerializeField] private float m_RunSpeed = 2;
    [SerializeField] private float m_JumpSpeed = 1;
    [SerializeField] private float m_StickToGroundForce = 1;
    [SerializeField] private float m_GravityMultiplier = 1;
    [SerializeField] private LerpControlledBob m_JumpBob = new LerpControlledBob();
    [SerializeField] public Transform m_Alvo;
    [SerializeField] private Camera m_CameraPrefab;
    private Renderer[] mesh;
    public bool m_Jump;
    private bool m_Jumptwo;
    private Vector2 m_Input;
    private Vector3 m_MoveDir = Vector3.zero;
    private CharacterController m_CharacterController;
    private CollisionFlags m_CollisionFlags;
    private bool m_PreviouslyGrounded;
    public bool m_Jumping;
    
    private bool m_Water = false;
    private Camera m_Camera;
    private zoom m_camera;
    private Transform Direcao=null;
	private Status m_Status;
    private bool FistPerson;

    void Start()
    {
        mesh = transform.GetComponentsInChildren<Renderer>();
        FistPerson = false;
        m_Camera = Instantiate<Camera>(m_CameraPrefab); ;
        m_camera = m_Camera.GetComponent<zoom>();
        m_camera.SetAlvo(m_Alvo);
        m_camera.Start();
        m_CharacterController = GetComponent<CharacterController>();
        m_Jumping = false;
        m_Status = GetComponent<Status>();

        if (!Direcao)
        {
            if (transform.Find("Direcao") != null)
            {
                Direcao = transform.Find("Direcao");
            }
            else
            {
                if (!Direcao)
                    Direcao = new GameObject("Direcao").transform;
                Direcao.transform.parent = transform;
            }
        }
    }

    private void Update()
    {
        if (!FistPerson && m_camera.GetDistancia() <1)
        {
            FistPerson = true;
            UpdateMesh(!FistPerson);
        }else if(FistPerson && m_camera.GetDistancia() > 1)
        {
            FistPerson = false;
            UpdateMesh(!FistPerson);
        }
        if (!m_PreviouslyGrounded && m_CharacterController.isGrounded)
        {
            StartCoroutine(m_JumpBob.DoBobCycle());
            m_MoveDir.y = 0f;
            m_Jumping = false;
        }
        if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
        {
            m_MoveDir.y = 0f;
        }

        m_PreviouslyGrounded = m_CharacterController.isGrounded;
        if (m_Status.GetVida()>0)
        {
            if (!m_Jump)
            {
                m_Jump = Input.GetButtonDown("Jump");
            }
            else
            {
                if (m_CharacterController.isGrounded)
                {
                    Debug.Log("reset");
                    GetComponent<Animator>().ResetTrigger("Pular");
                }
            }
        }
    }

    private void FixedUpdate()
	{
        float speed;
        GetInput(out speed);
        
        Vector3 desiredMove;
        desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;
        if (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift))
        {
            m_Input /= 2;
        }
        m_Status.SetInput(m_Input);
        RaycastHit hitInfo;
        Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out hitInfo,
                           m_CharacterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
        desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;
        if (hitInfo.collider != null)
        {
            if (hitInfo.collider.tag == "Agua")
                WalkWater();
        }
        m_MoveDir.x = desiredMove.x * speed;
        m_MoveDir.z = desiredMove.z * speed;
        
        if (m_CharacterController.isGrounded)
        {
            m_MoveDir.y = -m_StickToGroundForce;

            if (m_Jump)
            {
                if (m_Status.GetTypeAnimation(Type.Life))
                {
                    if (m_Status.SetTrigger("Pular"))
                    {
                        m_MoveDir.y = m_JumpSpeed;
                        m_Jumping = true;
                        m_Jumptwo = true;
                        m_Jump = false;
                    }
                }
            }
        }
        else
        {
            if (m_Jump && m_Jumping && m_Jumptwo)
            {
                if (m_Status.GetTypeAnimation(Type.Life))
                {
                    if (m_Status.SetTrigger("Pular"))
                    {
                        m_MoveDir.y = m_JumpSpeed;
                        m_Jump = false;
                        m_Jumping = true;
                        m_Jumptwo = false;
                    }
                }
            }
            m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
        }

        m_CollisionFlags = m_CharacterController.Move(m_MoveDir * Time.fixedDeltaTime);
        
        m_Status.Update(speed,m_CharacterController.velocity, m_CharacterController.isGrounded);

        if (m_Status.GetVida()>0) {
            m_Status.SetBool("chao", m_CharacterController.isGrounded);
            transform.eulerAngles = Vector3.up * m_Camera.transform.eulerAngles.y;
            if (m_Status.m_IsWalking)
            {
                m_WalkSpeed = (m_Status.GetVelocidadeDeMovimento());
            }
            else
            {
                m_RunSpeed = m_Status.GetVelocidadeDeMovimento()*2;
            }
            if (Mathf.Abs(m_Input.x) > Mathf.Abs(m_Input.y))
            {
                m_Status.SetFloat("Vertical", 0);
                m_Status.SetFloat("Horizontal", m_Input.x);
            }
            else
            {
                m_Status.SetFloat("Horizontal", 0);
                m_Status.SetFloat("Vertical", m_Input.y);
            }
        }

	}
    
    private void GetInput(out float speed)
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
#if !MOBILE_INPUT
        // On standalone builds, walk/run speed is modified by a key press.
        // keep track of whether or not the character is walking or running
        m_Status.m_IsWalking = !Input.GetKey(KeyCode.LeftShift);
#endif
        if (m_Status.GetTypeAnimation(Type.Move))
        {
            speed = m_Status.m_IsWalking ? m_WalkSpeed : m_RunSpeed;
            m_Input = new Vector2(horizontal, vertical);
        }
        else
        {
            speed = 0;
            m_Input = Vector2.zero;
        }
        if (m_Input.sqrMagnitude > 1)
        {
            m_Input.Normalize();
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;
        //dont move the rigidbody if the character is on top of it
        if (m_CollisionFlags == CollisionFlags.Below)
        {
            return;
        }

        if (body == null || body.isKinematic)
        {
            return;
        }
        body.AddForceAtPosition(m_CharacterController.velocity * 0.1f, hit.point, ForceMode.Impulse);
    }

    private void UpdateMesh(bool active)
    {
        for (int i = 0; i < mesh.Length; i++)
        {
            mesh[i].gameObject.SetActive(active);
        }
    }

    private void WalkWater()
    {
            RaycastHit hit;
            if (m_Water)
            {
                if (Physics.Raycast(transform.position, Vector3.up, out hit))
                {
                    if (hit.collider.tag == "Agua")
                    {
                        m_Water = true;
                    }
                    else
                    {
                        m_Water = false;
                        m_GravityMultiplier = 1;
                    }
                }
                else
                {
                    m_GravityMultiplier = 1;
                    m_Water = false;
                }
                if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
                {
                    m_GravityMultiplier = 0;
                }
                else
                {
                    m_GravityMultiplier = 0.2f;
                }
            }
    }

    public Transform GetHorbital(){
		return m_Camera.transform;
	}

    public static void instance (GameObject a,Transform b){
		instance (a, b);
	}
}