﻿using UnityEngine;

public class Respaw : MonoBehaviour {
    [SerializeField] private GameObject m_PreEnemy;
    private GameObject m_Enemy;
    float time;
    [SerializeField] private LayerMask m_Layer = new LayerMask();
    void Awake() {
        m_Enemy=Instantiate<GameObject>(m_PreEnemy, transform.position, transform.rotation, transform);
	}
    private void Update()
    {
        if (time > 0)
        {
            time -= Time.deltaTime;
        }
        if (time < 0)
        {
            if (m_Enemy == null)
            {
                RaycastHit[] hit = Physics.SphereCastAll(transform.position, 50, Vector3.down, 5, m_Layer, QueryTriggerInteraction.Ignore);
                if (hit.Length == 0)
                    m_Enemy = Instantiate<GameObject>(m_PreEnemy, transform.position, transform.rotation, transform);
            }
            time = 0;
        }
    }
    public void Insta () {
        time = 60;
    }
}
