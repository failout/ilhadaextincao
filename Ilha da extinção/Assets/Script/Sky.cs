﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;
public class Sky : MonoBehaviour {
    public enum Tempo { amanhece, entardece, manha, tarde, noite, madrugada }
    [SerializeField] private Skypie[] m_SkyMaterial;
    [SerializeField] private int _EscalaDeTempoPorSegudo = 120;
    [SerializeField] private int _VelocidadeDasNuvens = 1;
    [SerializeField] private float _OffSet = 0.4f;
    private Material _SkyMaterial = null;
    private float m_Tempo = 0;
    private Tempo m_HorarioDoDia;
    private string m_Horas;
    private float _TempoDaNuvem=0;
    private Light _Luz;
    void Start ()
    {
        GetHora(0, 0);
        _SkyMaterial = GetMaterialDoDia(m_HorarioDoDia);
        _Luz = GetComponent<Light>();
    }

    void FixedUpdate()
    {
        m_Tempo += _EscalaDeTempoPorSegudo * Time.deltaTime;
        int a = (int)m_Tempo % 1440;
        int b = (int)m_Tempo / 1440;
        a = a / 24;
        if (b + ":" + a != m_Horas)
        {
            Tempo time = m_HorarioDoDia;
            GetHora(a,b);
            if (time != m_HorarioDoDia)
            {
                _SkyMaterial=GetMaterialDoDia(m_HorarioDoDia);
            }
        }
        _TempoDaNuvem += _VelocidadeDasNuvens/10 * Time.deltaTime;
        double _horas =(double) 360 / 86400;
       _horas = _horas * m_Tempo - 180;
        if (m_Tempo > 86400)
            m_Tempo = 0;
        if (_TempoDaNuvem > 360)
            _TempoDaNuvem = 0;
        float _Dia = (float)1 / 180 * (float)_horas;
        if (_Dia < 0)
            _Dia = -1 * _Dia;
        transform.eulerAngles = new Vector3((float)_horas - 90, 0, 0);
        _SkyMaterial.SetFloat("_Rotation", _TempoDaNuvem);
        _Luz.intensity = _Dia + (_OffSet * _Dia);
        _SkyMaterial.SetFloat("_Exposure", _Dia + (_OffSet - (_OffSet * _Dia)));
        _TempoDaNuvem += _VelocidadeDasNuvens * Time.deltaTime;
        m_Tempo += _EscalaDeTempoPorSegudo * Time.deltaTime;
    }
    private Material GetMaterialDoDia(Tempo tempo)
    {
        List<Material> material = new List<Material>();
        for(int i = 0; i < m_SkyMaterial.Length; i++)
        {
            if (m_SkyMaterial[i].time== tempo)
            {
                material.Add(m_SkyMaterial[i].m_Materiais);
            }
        }
        if (material.Count == 0)
            return _SkyMaterial;
        return material[Random.Range(0, material.Count)];
    }
   private void GetHora(int a, int b)
    {
        m_Horas = b + ":" + a;
        switch (m_Horas)
        {
            case "1:00":
                m_HorarioDoDia = Tempo.madrugada;
                break;
            case "5:00":
                m_HorarioDoDia = Tempo.amanhece;
                break;
            case "7:00":
                m_HorarioDoDia = Tempo.manha;
                break;
            case "13:00":
                m_HorarioDoDia = Tempo.tarde;
                break;
            case "17:00":
                m_HorarioDoDia = Tempo.entardece;
                break;
            case "19:00":
                m_HorarioDoDia = Tempo.noite;
                break;
        }
    }
    public string GetHoras()
    {
        return m_Horas;
    }
    [Serializable]
    public struct Skypie
    {
        public Tempo time;
        public Material m_Materiais;
    }
}
