﻿using UnityEngine;

public enum Type { Idle, Atak, Move, Life, Death };
public struct status {
    public int atual, maximo, regeneracao;
}
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class Status : MonoBehaviour {
    private int m_vida=1000,m_VidaMax = 1000, m_Reg = 1, m_VelocidadeDemovimento=100;
    private float m_Time=0,m_RegTime=0,m_TempoMorto=0;
    private AnimatorStateInfo AnimatorInfor;
    private Animator m_Animator;
    [SerializeField]private Transform[] m_Inimigo;
    [SerializeField] private AudioClip[] m_FootstepSounds = new AudioClip[0];    // an array of footstep sounds that will be randomly selected from.
    [SerializeField] private AudioClip m_JumpSound = null;          // the sound played when character leaves the ground.
    [SerializeField] private AudioClip m_LandSound = null;         // the sound played when character touches back on ground.
    private AudioSource m_AudioSource;
    [SerializeField] private float m_StepInterval = 0;
    [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten = 1;
    public bool m_IsWalking;
    private float m_StepCycle;
    private float m_NextStep;
    private Vector2 m_Input;

    void Start () {
        m_Animator = GetComponent<Animator>();
        m_StepCycle = 0f;
        m_NextStep = m_StepCycle / 2f;

        m_AudioSource = GetComponent<AudioSource>();
        AnimatorInfor = m_Animator.GetCurrentAnimatorStateInfo(0);
    }
    
    void Update () {

        PlayLandingSound();
        if (m_TempoMorto > 0)
            m_TempoMorto -= Time.deltaTime;
        if (m_TempoMorto < 0)
        {
            m_vida = m_VidaMax;
            m_TempoMorto = 0;
        }
        if (m_Time > 0)
            m_Time -= Time.deltaTime;
        if (m_vida < m_VidaMax)
            m_RegTime += Time.deltaTime;
        if (m_RegTime > 1)
        {
            m_RegTime--;
            m_vida=m_Reg;
        }
    }
    public void Update(float speed,Vector3 a,bool isGrounded)
    {
        ProgressStepCycle(speed,a, isGrounded);
    }

    public int GetVelocidadeDeMovimento()
    {
        return m_VelocidadeDemovimento/100;
    }

    public float GetTime()
    {
        return m_Time;
    }

    public bool SetTrigger(string anim)
    {
        if("Pular"==anim)
            PlayJumpSound();
        m_Animator.SetTrigger(anim);
        return true;
    }
    public bool SetBool(string anim,bool a)
    {
        m_Animator.SetBool(anim,a);
        return true;
    }

    public void SetDano(int dano,Transform alvo)
    {
        if (m_vida - dano <= 0)
        {
            m_TempoMorto = 200;
        }
        m_Time = 30;
        m_vida -= dano;
    }

    public bool SetFloat(string anim, float a)
    {
        m_Animator.SetFloat(anim, a);
        return true;
    }

    public Transform[] GetInimigo()
    {
        return m_Inimigo;
    }

    public bool GetTypeAnimation(Type tipo)
    {
        AnimatorInfor = m_Animator.GetCurrentAnimatorStateInfo(0);
        if (tipo == Type.Move)
        {
            if (AnimatorInfor.IsTag("AtakIdle") || AnimatorInfor.IsTag("Death") || AnimatorInfor.IsTag("DefenceIdle"))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        if (tipo == Type.Atak)
        {
            if (AnimatorInfor.IsTag("AtakMove") || AnimatorInfor.IsTag("AtakIdle") || AnimatorInfor.IsTag("Atak"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        if (tipo == Type.Life)
        {
            if (AnimatorInfor.IsTag("Death"))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        return true;
    }

    public int GetVida()
    {
        return m_vida;
    }

    private void PlayLandingSound()
    {
        m_AudioSource.clip = m_LandSound;
        m_AudioSource.Play();
        m_NextStep = m_StepCycle + .5f;
    }
    
    private void ProgressStepCycle(float speed,Vector3 velocity, bool isGrounded)
    {
        if (velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
        {
            m_StepCycle += (velocity.magnitude + (speed * (m_IsWalking ? 1f : m_RunstepLenghten))) *
                        Time.fixedDeltaTime;
        }

        if (!(m_StepCycle > m_NextStep))
        {
            return;
        }

        m_NextStep = m_StepCycle + m_StepInterval;

        PlayFootStepAudio(isGrounded);
    }
    
    private void PlayJumpSound()
    {
        m_AudioSource.clip = m_JumpSound;
        m_AudioSource.Play();
    }

    public void SetInput(Vector2 input)
    {
        m_Input = input;
    }

    private void PlayFootStepAudio(bool isGrounded)
    {
        if (!isGrounded)
        {
            return;
        }
        if (m_FootstepSounds.Length == 0)
            return;
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, m_FootstepSounds.Length);
        m_AudioSource.clip = m_FootstepSounds[n];
        m_AudioSource.PlayOneShot(m_AudioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        m_FootstepSounds[n] = m_FootstepSounds[0];
        m_FootstepSounds[0] = m_AudioSource.clip;
    }

}
