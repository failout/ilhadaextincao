﻿using UnityEngine;

[ExecuteInEditMode]
public class zoom : MonoBehaviour
{
    [SerializeField] private Transform alvo;
    [SerializeField] private float offSet = 1.8f;
    [SerializeField] private float dist = 1.8f;
    [SerializeField] private float vel = 10;
    [SerializeField] private float DistMax=50;
    [SerializeField] private float XSensitivity = 2f;
    [SerializeField] private float YSensitivity = 2f;
    [SerializeField] private bool clampVerticalRotation = true;
    [SerializeField] private float MinimumX = -90F;
    [SerializeField] private float MaximumX = 90F;
    [SerializeField] private bool smooth=false;
    [SerializeField] private float smoothTime = 5f;
    [SerializeField] private LayerMask m_LayerMask;
    RaycastHit hit = new RaycastHit ();

    private Quaternion m_CameraTargetRotX;
    private bool m_cursorIsLocked = false;
    public void Start ()
	{
        if (alvo)
            Init(alvo.transform, transform);
    }

	void FixedUpdate ()
	{
        if (!alvo)
            return;
        UpdateCursorLock();
        dist -= Input.GetAxis("Mouse ScrollWheel") * vel * Time.deltaTime;
        if (dist > 1)
        {
            transform.RotateAround(alvo.position, transform.right, -Input.GetAxis("Mouse Y") * 90 * Time.deltaTime);
            transform.RotateAround(alvo.position, transform.up, Input.GetAxis("Mouse X") * 90 * Time.deltaTime);

            if (transform.eulerAngles.y >= 360)
                transform.eulerAngles = Vector3.zero;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0);
            transform.position = alvo.position - transform.forward * dist;

            if (dist > DistMax)
                dist = DistMax;
            if (Physics.Linecast(alvo.position, transform.position, out hit, m_LayerMask))
            {
                transform.position = hit.point;
            }
        }
        else
        {
            if (dist< 0.5f)
                dist = 0.5f;
            transform.position = alvo.position + (Vector3.up * offSet);
            LookRotation(transform);

            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0);
            m_CameraTargetRotX = transform.rotation;
        }
	}

    public float GetDistancia()
    {
        return dist;
    }
    
    public void SetAlvo(Transform target)
    {
        alvo = target;
    }

    public void LookRotation(Transform camera)
    {
        float yRot = Input.GetAxis("Mouse X") * XSensitivity;
        float xRot = Input.GetAxis("Mouse Y") * YSensitivity;
        
        m_CameraTargetRotX *= Quaternion.Euler(-xRot, yRot, 0f);

        if (clampVerticalRotation)
        {
            m_CameraTargetRotX = ClampRotationAroundXAxis(m_CameraTargetRotX);
        }

        if (smooth)
        {
            camera.rotation = Quaternion.Slerp(camera.rotation, m_CameraTargetRotX, smoothTime * Time.deltaTime);
        }
        else
        {
            camera.rotation = m_CameraTargetRotX;
        }
    }

    public void UpdateCursorLock()
    {
            InternalLockUpdate();
    }

    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, MinimumX, MaximumX);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);
        return q;
    }

    private void InternalLockUpdate()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            m_cursorIsLocked = false;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (dist < 1)
                m_cursorIsLocked = true;
            else
            {
                m_cursorIsLocked = false;
            }
        }
        if (m_cursorIsLocked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else //if (!m_cursorIsLocked)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void SetCursorLock(bool value)
    {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
    }

    public void Init(Transform character, Transform camera)
    {
        m_CameraTargetRotX = camera.localRotation;
    }
}
